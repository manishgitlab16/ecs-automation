resource "aws_ecs_cluster" "web-cluster" {
  name = var.cluster_name
  tags = {
    "env" = "dev"
  }
}

//data "template_file" "api_container_definitions" {
//  template = file("container-definitions/container-definitions.json.tpl")
//}




# update file container-def, so it's pulling image from ecr
resource "aws_ecs_task_definition" "task-definition-test" {
  family = "web-family"
  //container_definitions = data.template_file.api_container_definitions.rendered
  container_definitions = file("container-definitions/container-definitions.json")
  network_mode          = "bridge"
  tags = {
    "env" = "dev"
  }
}

resource "aws_ecs_service" "service" {
  name            = "web-service"
  cluster         = aws_ecs_cluster.web-cluster.id
  task_definition = aws_ecs_task_definition.task-definition-test.arn
  desired_count   = 2
  ordered_placement_strategy {
    type  = "binpack"
    field = "cpu"
  }


  load_balancer {
    target_group_arn = aws_lb_target_group.target_group.arn
    container_name   = "dish-poc"
    container_port   = 80
  }
  # Optional: Allow external changes without Terraform plan difference(for example ASG)
  lifecycle {
    ignore_changes = [desired_count]
  }
  launch_type = "EC2"
  depends_on  = [aws_lb_listener.web_listener, aws_iam_role_policy_attachment.ecs-instance-role-attachment]
}


