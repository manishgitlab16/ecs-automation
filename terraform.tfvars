################################### EC2 Generic instance values ###################################
aws_region           = "us-east-1"
ec2_image            = "ami-043379e71bae59340"
ec2_keypair          = "MyKey"
count_id             = "1"
subnet_id            = "subnet-1901e938"
ec2_sec_group        = ["sg-06ba668d094ce44b0"]
instance_flavor_type = "t3.medium"
instance_tag         = "ec2_terraform_test"

###################################  ELB values   ##################################################

lb_name              = "test-web-lb"
lb_internal_type     = "true"
lb_loadbalancer_type = "application"
lb_sec_group         = ["sg-06ba668d094ce44b0"]
subnets              = ["subnet-f5aa4793", "subnet-1872cf55"]
lb_protection        = "false"
ip_address_type      = "ipv4"
lb_tag_name          = "alb_terraform_test"

###################################  ALB Target Group values #######################################
lb_target_group_interval            = "10"
lb_target_group_path                = "/"
lb_target_group_timeout             = "5"
lb_target_group_healthy_threshold   = "5"
lb_target_group_unhealthy_threshold = "2"

lb_target_group_name         = "tf-example-lb-tg"
lb_target_type_instance_port = "8080"
lb_protocol                  = "HTTP"
existing_vpc_id              = "vpc-0681bf7c"
//alb_target_group_type                 = "ip"  // "ip" or "instance" by default


###################################  ELB Listener variables ##########################################
lb_listerner_port    = "80"
lb_listener_protocol = "HTTP"
//lb_listener_ssl_policy                = "ELBSecurityPolicy-2016-08"
//lb_listener_certificate_arn           = "arn:aws:iam::1234567890:server-certificate/test_cert_wanodnownofndonwofjwi299"
lb_listener_default_action_type = "forward"
lb_target_group_attachment_port = "8080"





